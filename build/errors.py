from enum import Enum


errors = {
    "bad_url": (
        "ERR001: Vous devez fournir l'URL d'un dépôt de code public.",
        "ERR001: Not an URL",
    ),
    "limit_per_24h": (
        "ERR002: Le nombre de générations par jour est limité à {0}.",
        "ERR002: Too much generation",
    ),
    "vcs_not_supported": (
        "ERR101: Le système de version {0} n'est pas encore supporté.",
        "ERR101: Version Control System not supported",
    ),
    "unknown_vcs": (
        "ERR102: Le système de version n'est pas reconnu.",
        "ERR102: Unknown version control system",
    ),
    "timeout_git": (
        "ERR103: Délai d'attente lors du test git dépassé.",
        "ERR103: Timeout while testing repository against git",
    ),
    "timeout_mercurial": (
        "ERR104: Délai d'attente lors du test mercurial dépassé.",
        "ERR104: Timeout while testing repository against hg",
    ),
    "timeout_clone": (
        "ERR105: Délai d'attente lors de la récupération du dépôt dépassé.",
        "ERR105: Timeout while cloning repository",
    ),
    "error_clone": (
        "ERR106: Erreur lors de la récupération du dépôt.",
        "ERR106: Cloning repository failed",
    ),
    "repository_size": (
        "ERR107: La taille du dépôt est trop grande.",
        "ERR107: Repository size too large",
    ),
    "forbidden_domain": (
        "ERR201: Le domaine {0} est interdit.",
        "ERR201: Domain name forbidden",
    ),
    "generator_not_supported": (
        "ERR401: Le générateur de site {0} n'est pas encore supporté.",
        "ERR401: generator not supported",
    ),
    "generator_unknown": (
        "ERR402: Le générateur de site n'est pas reconnu.",
        "ERR402: No supported website generator found",
    ),
    "timeout_ruby_version": (
        "ERR500: Délai d'attente de vérification de la version de ruby dépassé.",
        "ERR500: Timeout while listing ruby versions",
    ),
    "timeout_ruby_install": (
        "ERR501: Délai d'attente de l'installation de "
        "la version de ruby {0} dépassé.",
        "ERR501: Timeout during ruby installation",
    ),
    "error_ruby_install": (
        "ERR502: Erreur lors de l'installation de ruby {0}",
        "ERR502: Error while installing ruby",
    ),
    "imeout_ruby_init": (
        "ERR503: Délai d'attente de l'initialisationn de ruby dépassé.",
        "ERR503: Timeout while initializing ruby version",
    ),
    "timeout_tools_verification": (
        "ERR504: Délai d'attente de la vérification des outils dépassé.",
        "ERR504: Timeout while verifying available tools versions",
    ),
    "timeout_bundler_install": (
        "ERR505: Délai d'attente de l'installation de bundler dépassé.",
        "ERR505: Timeout while installing bundler",
    ),
    "timeout_jekyll_init": (
        "ERR506: Délai d'attente de l'initialisation de jekyll dépassé.",
        "ERR506: Timeout while initializing jekyll",
    ),
    "error_jekyll_init": (
        "ERR507: Erreur pendant l'initialisation de jekyll.",
        "ERR507: Error while initializing jekyll",
    ),
    "timeout_install_dependencies": (
        "ERR508: Délai d'attente lors de l'installation des dépendances dépassé.",
        "ERR508: Timeout while installing dependencies",
    ),
    "error_install_dependencies": (
        "ERR509: Erreur lors de l'installation des dépendances.",
        "ERR509: Error while installing dependencies",
    ),
    "timeout_hugo_install": (
        "ERR501: Délai d'attente de l'installation de "
        "la version de hugo {0} dépassé.",
        "ERR501: Timeout during hugo installation",
    ),
    "error_hugo_install": (
        "ERR502: Erreur lors de l'installation de hugo {0}",
        "ERR502: Error while installing hugo",
    ),
    "timeout_postcss_install": (
        "ERR503: Délai d'attente de l'installation de postcss-cli dépassé.",
        "ERR503: Timeout during postcss-cli installation",
    ),
    "timeout_build": (
        "ERR601: Délai d'attente lors de la génération du site dépassé.",
        "ERR601: Timeout while building website",
    ),
    "error_build": (
        "ERR602: Erreur lors de la génération du site.",
        "ERR602: Error while building website",
    ),
    "unknown_error_build": (
        "ERR603: Erreur lors de la génération du site.",
        "ERR603: Error while building website: nothing built",
    ),
    "result_size": (
        "ERR604: Le site généré est trop lourd.",
        "ERR604: Built website is too large",
    ),
    "timeout_copy": (
        "ERR701: Délai d'attente lors de la copie des fichiers dépassé.",
        "ERR701: Timeout while copying files",
    ),
    "timeout_certificate": (
        "ERR702: Délai d'attente de la génération du certificat dépassé.",
        "ERR702: Timeout while generating certificate",
    ),
    "error_certificate": (
        "ERR703: Erreur durant la génération du certificat.",
        "ERR703: Error while generating certificate",
    ),
    "error_nginx": (
        "ERR704: Erreur durant la configuration du site web.",
        "ERR704: Error while configuring nginx",
    ),
    "error_size": (
        "ERR991: Erreur lors du calcul de la taille d'un répertoire.",
        "ERR991: Error while computing folder size",
    ),
    "unknown_error": (
        "ERR999: Une erreur inconnue est survenue.",
        "ERR999: Unknown error",
    ),
}


class ArdoiseError(Exception):
    def __init__(self, message, log, stacktrace=None):
        self.message = message
        self.log = log
        self.stacktrace = stacktrace
        super().__init__(self, message, stacktrace)


def raise_error(error_code, *args, stacktrace=None):
    if error_code not in errors:
        raise_error("unknown_error")

    if len(args) > 0:
        message = errors[error_code][0].format(*args)
    else:
        message = errors[error_code][0]
    raise ArdoiseError(message, errors[error_code][1], stacktrace)
