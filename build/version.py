from itertools import zip_longest
import re

SIMPLE_VERSION_PATTERN = r"^[0-9\.]+$"
MINIMAL_VERSION_PATTERN = r"^[~>!=]+\s+[0-9\.]+$"
MAXIMAL_VERSION_PATTERN = r"^[<!=]+\s+[0-9\.]+$"


class Version:
    version: str

    def __init__(self, version):
        self.components = version.split(".")
        self.nb_components = len(self.components)

    @property
    def version(self):
        return ".".join(self.components)

    @property
    def major(self):
        return self[0]

    @property
    def minor(self):
        return self[1]

    @property
    def patch(self):
        return self[2]

    def __getitem__(self, index):
        try:
            if self.nb_components >= index + 1:
                return int(self.components[index])
        except ValueError:
            # Certainly some "rc" or "alpha" thing, let's use a simple 0 in place
            pass
        return 0

    def __lt__(self, other):
        nb_components = max(self.nb_components, other.nb_components)
        for idx in range(nb_components):
            if self[idx] < other[idx]:
                return True
            elif self[idx] > other[idx]:
                return False
        return False

    def __gt__(self, other):
        nb_components = max(self.nb_components, other.nb_components)
        for idx in range(nb_components):
            if self[idx] > other[idx]:
                return True
            elif self[idx] < other[idx]:
                return False
        return False

    def __ne__(self, other):
        nb_components = max(self.nb_components, other.nb_components)
        for idx in range(nb_components):
            if self[idx] != other[idx]:
                return True
        return False

    def __le__(self, other):
        return not self.__gt__(other)

    def __ge__(self, other):
        return not self.__lt__(other)

    def __eq__(self, other):
        return not self.__ne__(other)

    def __repr__(self):
        return self.version

    def next(self):
        next_version = Version(self.version)
        next_version.components[-1] = str(int(next_version.components[-1]) + 1)
        return next_version


class RubyGemVersionConstraint:
    def __init__(self, constraint):
        self.min_version = None
        self.min_equal = False
        self.max_version = None
        self.max_equal = False
        self.blacklist = []
        self.analyze(constraint)

    def analyze(self, constraint):
        constraints = constraint.split(", ")
        for constraint in constraints:
            if "-" in constraint:
                # Semantic version ?
                constraint = constraint.split("-")[0]
            if re.match(SIMPLE_VERSION_PATTERN, constraint):
                self.min_version = Version(constraint)
                self.max_version = Version(constraint)
            elif re.match(MINIMAL_VERSION_PATTERN, constraint):
                if "!=" in constraint:
                    self.blacklist.append(Version(constraint.split()[1]))
                else:
                    self.min_version = Version(constraint.split()[1])
                    if "=" in constraint:
                        self.min_equal = True
                    if "~" in constraint:
                        self.min_equal = True
                        max_version = Version(self.min_version.version)
                        if max_version.nb_components > 1:
                            max_version.components.pop()
                        self.max_version = max_version.next()
            elif re.match(MAXIMAL_VERSION_PATTERN, constraint):
                if "!=" in constraint:
                    self.blacklist.append(Version(constraint.split()[1]))
                else:
                    self.max_version = Version(constraint.split()[1])
                    if "=" in constraint:
                        self.max_equal = True

    def __repr__(self):
        result = f"{self.min_version} <"
        if self.min_equal:
            result += "="
        result += " v <"
        if self.max_equal:
            result += "="
        result += f" {self.max_version}"

        return result

    def __contains__(self, version):
        if version in self.blacklist:
            return False

        if str(self.min_version) == str(self.max_version):
            return self.min_version == version

        return self.compare_minimal(version) and self.compare_maximal(version)

    def compare_minimal(self, version):
        if self.min_version is None:
            return True

        if self.min_equal:
            return self.min_version <= version
        else:
            return self.min_version < version

    def compare_maximal(self, version):
        if self.max_version is None:
            return True

        if self.max_equal:
            return version <= self.max_version
        else:
            return version < self.max_version


if __name__ == "__main__":
    assert Version("0") < Version("1")
    assert Version("0") < Version("0.1")
    assert Version("0") < Version("0.0.1")
    assert Version("0.7") < Version("1")
    assert Version("0.7") < Version("0.8")
    assert Version("0.7") < Version("0.7.5")
    assert Version("0.7.4") < Version("1")
    assert Version("0.7.4") < Version("0.8")
    assert Version("0.7.4") < Version("0.7.5")

    assert Version("0") <= Version("1")
    assert Version("0") <= Version("0.1")
    assert Version("0") <= Version("0.0.1")
    assert Version("0.7") <= Version("1")
    assert Version("0.7") <= Version("0.8")
    assert Version("0.7") <= Version("0.7.5")
    assert Version("0.7.4") <= Version("1")
    assert Version("0.7.4") <= Version("0.8")
    assert Version("0.7.4") <= Version("0.7.5")

    assert Version("0") <= Version("0")
    assert Version("0") <= Version("0.0")
    assert Version("0") <= Version("0.0.0")
    assert Version("0.7") <= Version("0.7")
    assert Version("0.7") <= Version("0.7.0")
    assert Version("0.7.4") <= Version("0.7.4")
    assert Version("0.7.4") <= Version("0.7.4.0")

    assert Version("2") > Version("1")
    assert Version("2") > Version("1.1")
    assert Version("2") > Version("1.0.1")
    assert Version("2.7") > Version("2")
    assert Version("2.7") > Version("2.6")
    assert Version("2.7") > Version("2.6.9")
    assert Version("2.7.4") > Version("2")
    assert Version("2.7.4") > Version("2.7")
    assert Version("2.7.4") > Version("2.7.3")

    assert Version("2") >= Version("1")
    assert Version("2") >= Version("1.1")
    assert Version("2") >= Version("1.0.1")
    assert Version("2.7") >= Version("2")
    assert Version("2.7") >= Version("2.6")
    assert Version("2.7") >= Version("2.6.9")
    assert Version("2.7.4") >= Version("2")
    assert Version("2.7.4") >= Version("2.7")
    assert Version("2.7.4") >= Version("2.7.3")

    assert Version("2") >= Version("2")
    assert Version("2") >= Version("2.0")
    assert Version("2") >= Version("2.0.0")
    assert Version("2.7") >= Version("2.7")
    assert Version("2.7") >= Version("2.7.0")
    assert Version("2.7.4") >= Version("2.7.4")
    assert Version("2.7.4") >= Version("2.7.4.0")

    assert Version("1.2.3").major == 1
    assert Version("1.2.3").minor == 2
    assert Version("1.2.3").patch == 3

    assert Version("3").major == 3
    assert Version("3")[0] == 3
    assert Version("3").minor == 0
    assert Version("3")[1] == 0
    assert Version("3").patch == 0
    assert Version("3")[2] == 0
    assert Version("3")[3] == 0

    rgvc = RubyGemVersionConstraint(">= 0.7, < 2")
    assert Version("2.0") not in rgvc

    rgvc = RubyGemVersionConstraint("1.4.0")
    assert Version("0.1.0") not in rgvc
    assert Version("1.3.0") not in rgvc
    assert Version("1.4.0") in rgvc
    assert Version("1.4.1") not in rgvc
    assert Version("1.5.0") not in rgvc
    assert Version("2.4.0") not in rgvc

    rgvc = RubyGemVersionConstraint(">= 1.4.0")
    assert Version("0.1.0") not in rgvc
    assert Version("1.3.0") not in rgvc
    assert Version("1.4.0") in rgvc
    assert Version("1.4.1") in rgvc
    assert Version("1.5.0") in rgvc
    assert Version("2.4.0") in rgvc

    rgvc = RubyGemVersionConstraint("> 1.4.0")
    assert Version("0.1.0") not in rgvc
    assert Version("1.3.0") not in rgvc
    assert Version("1.4.0") not in rgvc
    assert Version("1.4.1") in rgvc
    assert Version("1.5.0") in rgvc
    assert Version("2.4.0") in rgvc

    rgvc = RubyGemVersionConstraint("<= 1.4.0")
    assert Version("0.1.0") in rgvc
    assert Version("1.3.0") in rgvc
    assert Version("1.4.0") in rgvc
    assert Version("1.4.1") not in rgvc
    assert Version("1.5.0") not in rgvc
    assert Version("2.4.0") not in rgvc

    rgvc = RubyGemVersionConstraint("< 1.4.0")
    assert Version("0.1.0") in rgvc
    assert Version("1.3.0") in rgvc
    assert Version("1.4.0") not in rgvc
    assert Version("1.4.1") not in rgvc
    assert Version("1.5.0") not in rgvc
    assert Version("2.4.0") not in rgvc

    rgvc = RubyGemVersionConstraint(">= 0.7, <= 2")
    assert Version("0.5") not in rgvc
    assert Version("0.7") in rgvc
    assert Version("0.9") in rgvc
    assert Version("1.4") in rgvc
    assert Version("2") in rgvc
    assert Version("2.0") in rgvc
    assert Version("2.0.0") in rgvc
    assert Version("2.0.1") not in rgvc
    assert Version("2.1") not in rgvc

    rgvc = RubyGemVersionConstraint(">= 0.7, < 2")
    assert Version("0.5") not in rgvc
    assert Version("0.7") in rgvc
    assert Version("0.9") in rgvc
    assert Version("1.4") in rgvc
    assert Version("2") not in rgvc
    assert Version("2.0") not in rgvc
    assert Version("2.0.0") not in rgvc
    assert Version("2.0.1") not in rgvc
    assert Version("2.1") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2.2")
    assert Version("0.5") not in rgvc
    assert Version("1.4") not in rgvc
    assert Version("2") not in rgvc
    assert Version("2.0") not in rgvc
    assert Version("2.0.0") not in rgvc
    assert Version("2.0.1") not in rgvc
    assert Version("2.1") not in rgvc
    assert Version("2.2.0") in rgvc
    assert Version("2.2.1") in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2.2, >= 2.2.1")
    assert Version("2.2") not in rgvc
    assert Version("2.2.0") not in rgvc
    assert Version("2.2.1") in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2.2")
    assert Version("2.2") in rgvc
    assert Version("2.2.0") in rgvc
    assert Version("2.2.1") in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("2.3.0") in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2.2.0")
    assert Version("2.2") in rgvc
    assert Version("2.2.0") in rgvc
    assert Version("2.2.1") in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("2.3.0") not in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 3.0.3")
    assert Version("2.2") not in rgvc
    assert Version("3.0.0") not in rgvc
    assert Version("3.0.1") not in rgvc
    assert Version("3.0.3") in rgvc
    assert Version("3.0.5") in rgvc
    assert Version("3.1.3") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2")
    assert Version("1.2") not in rgvc
    assert Version("2.0.0") in rgvc
    assert Version("2.2.1") in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 2, != 2.2.1")
    assert Version("0.5") not in rgvc
    assert Version("1.4") not in rgvc
    assert Version("2") in rgvc
    assert Version("2.0") in rgvc
    assert Version("2.0.0") in rgvc
    assert Version("2.0.1") in rgvc
    assert Version("2.1") in rgvc
    assert Version("2.2.0") in rgvc
    assert Version("2.2.1") not in rgvc
    assert Version("2.2.12") in rgvc
    assert Version("2.2.2") in rgvc
    assert Version("3.0.0") not in rgvc

    rgvc = RubyGemVersionConstraint("~> 3.0")
    assert Version("4.0.0") not in rgvc
    assert Version("4.0.0.rc3") not in rgvc

    rgvc = RubyGemVersionConstraint("1.14.3-arm64-darwin")
    assert Version("1.13.0") not in rgvc
    assert Version("1.14.3") in rgvc
    assert Version("1.16.0") not in rgvc
