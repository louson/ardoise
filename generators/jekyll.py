"""
This module is used to build a static website with jekyll.
"""

import logging
import os
import re
import subprocess
import time

import requests

from build.errors import raise_error
from build.version import RubyGemVersionConstraint, Version
import settings

RUBY_API = "https://api.github.com/repos/ruby/ruby/git/matching-refs/tags"
RUBYGEMS_API = "https://rubygems.org/api/v1/versions/{}.json"

logging.getLogger("requests").setLevel(logging.WARNING)


def is_jekyll(context):
    for folder in settings.GENERATOR_FOLDERS:
        new_path = os.path.join(context.source_path, folder)
        if os.path.exists(os.path.join(new_path, ".nojekyll")):
            return False
        if os.path.exists(os.path.join(new_path, "_config.yml")):
            context.logger.info(f"Found something to build in '{new_path}'")
            context.source_path = new_path
            return True


class Jekyll:
    def __init__(self, context):
        self.name = "jekyll"
        self.context = context
        self.github_pages = False
        self.ruby_version = None
        self.bundler_version = None
        self.jekyll_version = None

        # Verify if this is a github page
        if "github" in self.context.repository.lower():
            self.github_pages = True

    def get_versions(self):
        # Retrieve specified ruby version, if any
        if self.github_pages:
            self.ruby_version = settings.JEKYLL_GITHUB_RUBY_VERSION
        else:
            self.ruby_version = settings.JEKYLL_DEFAULT_RUBY_VERSION
        ruby_version_path = os.path.join(self.context.source_path, ".ruby-version")
        gemfile_lock_path = os.path.join(self.context.source_path, "Gemfile.lock")
        if os.path.exists(ruby_version_path):
            with open(ruby_version_path) as file_handle:
                self.ruby_version = file_handle.read().splitlines()[0]
            self.context.logger.info(f"Ruby version specified: {self.ruby_version}")
        else:
            if os.path.exists(gemfile_lock_path):
                self.context.logger.info("Gemfile.lock present")

                self.context.logger.info("Retrieving components")
                specs_flag = False
                components = {}
                with open(gemfile_lock_path) as file_handle:
                    content = file_handle.read().splitlines()
                    for line in content:
                        if re.search(r"  specs:", line) is not None:
                            specs_flag = True
                        if specs_flag:
                            result = re.search(r"    ([A-Za-z0-9-]+) \(([^)]+)\)", line)
                            if result is not None:
                                component = result.group(1)
                                versions = result.group(2)
                                components[component] = RubyGemVersionConstraint(
                                    versions
                                )

                self.context.logger.info("Checking ruby constraints")
                ruby_constraints = [RubyGemVersionConstraint(">= 2.6.3")]
                for component in components:
                    result = requests.get(RUBYGEMS_API.format(component))
                    if result.status_code == 200:
                        versions = result.json()
                        for version in versions:
                            if (
                                Version(version["number"]) in components[component]
                                and version["ruby_version"] is not None
                            ):
                                ruby_constraints.append(
                                    RubyGemVersionConstraint(version["ruby_version"])
                                )
                                break
                    time.sleep(0.2)

                self.context.logger.info("Loading ruby versions")
                ruby_versions = []
                result = requests.get(RUBY_API)
                if result.status_code == 200:
                    versions = result.json()
                    for version in versions:
                        ruby_version = version["ref"].split("/")[-1][1:]
                        ruby_version = ruby_version.replace("_", ".")
                        ruby_versions.append(Version(ruby_version))

                self.context.logger.info("Merging constraints")
                for ruby_constraint in ruby_constraints:
                    to_remove = []
                    for ruby_version in ruby_versions:
                        if ruby_version not in ruby_constraint:
                            to_remove.append(ruby_version)
                    for version_to_remove in to_remove:
                        ruby_versions.remove(version_to_remove)

                self.context.logger.info("Checking available ruby versions")
                try:
                    result = subprocess.run(
                        "scripts/ruby_list.sh",
                        capture_output=True,
                        timeout=settings.TIMEOUT_RUBY_CHECK,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_ruby_version")

                available_ruby_versions = []
                content = result.stdout.decode("utf-8").splitlines()
                for line in content:
                    result = re.search(r"\s+([0-9]+\.[0-9]+\.[0-9]+)\s*", line)
                    if result is not None:
                        available_ruby_versions.append(result.group(1))
                self.context.logger.info(
                    f"Actual ruby versions found: {available_ruby_versions}"
                )

                for ruby_version in ruby_versions:
                    if ruby_version.version in available_ruby_versions:
                        self.ruby_version = ruby_version.version
                        break
                else:
                    self.ruby_version = ruby_versions[0].version

                self.context.logger.info(f"Ruby version to use: {self.ruby_version}")
            else:
                self.context.logger.info(
                    f"Default ruby version to use: {self.ruby_version}"
                )

        # Retrieve specified bundler and jekyll versions, if any
        if self.github_pages:
            self.bundler_version = settings.JEKYLL_GITHUB_BUNDLER_VERSION
            self.jekyll_version = settings.JEKYLL_GITHUB_JEKYLL_VERSION
        else:
            self.bundler_version = settings.JEKYLL_DEFAULT_BUNDLER_VERSION
            self.jekyll_version = settings.JEKYLL_DEFAULT_JEKYLL_VERSION
        bundler_flag = False
        if os.path.exists(gemfile_lock_path):
            with open(gemfile_lock_path) as file_handle:
                content = file_handle.read().splitlines()
                for line in content:
                    result = re.search(r"jekyll \([>=< ]*([0-9.]+)\)", line)
                    if result is not None:
                        self.jekyll_version = result.group(1)
                    if bundler_flag:
                        self.bundler_version = line.strip()
                        bundler_flag = False
                    if line.strip().lower() == "bundled with":
                        bundler_flag = True
            self.context.logger.info(
                f"Bundler version specified: {self.bundler_version}"
            )
            self.context.logger.info(f"Jekyll version specified: {self.jekyll_version}")
        else:
            self.context.logger.info(
                f"Default bundler version to use: {self.bundler_version}"
            )
            self.context.logger.info(
                f"Default jekyll version to use: {self.jekyll_version}"
            )

        # If jekyll is too old, maybe bundler should be adapted
        # TODO: Use version comparison
        pass

        # If bundler is too old, maybe ruby should be adapted
        # TODO: Use version comparison
        pass

    def get_build_ready(self):
        """
        With retrieved information, install needed tools.
        """
        if self.ruby_version is not None:
            self.context.logger.info("Checking available ruby versions")
            try:
                result = subprocess.run(
                    "scripts/ruby_list.sh",
                    capture_output=True,
                    timeout=settings.TIMEOUT_RUBY_CHECK,
                )
            except subprocess.TimeoutExpired:
                raise_error("timeout_ruby_version")

            ruby_versions = []
            content = result.stdout.decode("utf-8").splitlines()
            for line in content:
                result = re.search(r"\s+([0-9]+\.[0-9]+\.[0-9]+)\s*", line)
                if result is not None:
                    ruby_versions.append(result.group(1))
            self.context.logger.info(f"Actual ruby versions found: {ruby_versions}")

            if self.ruby_version not in ruby_versions:
                self.context.logger.info(f"Installing ruby version {self.ruby_version}")
                try:
                    result = subprocess.run(
                        ["scripts/ruby_install.sh", self.ruby_version],
                        capture_output=True,
                        timeout=settings.TIMEOUT_RUBY_INSTALL,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_ruby_install", self.ruby_version)

                if result.stderr.decode("utf-8") != "":
                    raise_error(
                        "error_ruby_install",
                        self.ruby_version,
                        stacktrace=result.stderr.decode("utf-8"),
                    )

                self.context.logger.info(f"Ruby version {self.ruby_version} installed")
            else:
                self.context.logger.info(f"Ruby version {self.ruby_version} found")

            # Specifying ruby version to use for further commands
            try:
                result = subprocess.run(
                    [
                        "scripts/rbenv_init.sh",
                        self.context.source_path,
                        self.ruby_version,
                    ],
                    capture_output=True,
                    timeout=settings.TIMEOUT_RUBY_INIT,
                )
            except subprocess.TimeoutExpired:
                raise_error("timeout_ruby_init")

        if self.bundler_version is not None:
            self.context.logger.info(f"Checking available bundler versions")
            try:
                result = subprocess.run(
                    ["scripts/gem_list.sh", self.context.source_path, "bundler"],
                    capture_output=True,
                    timeout=settings.TIMEOUT_BUNDLER_CHECK,
                )
            except subprocess.TimeoutExpired:
                raise_error("timeout_tools_verification")

            bundler_versions = re.findall(r"([0-9.]+)", result.stdout.decode("utf-8"))
            self.context.logger.info(
                f"Actual bundler versions found: {bundler_versions}"
            )
            if self.bundler_version not in bundler_versions:
                self.context.logger.info(
                    f"Installing bundler version {self.bundler_version}"
                )
                try:
                    result = subprocess.run(
                        [
                            "scripts/gem_install.sh",
                            self.context.source_path,
                            "bundler",
                            self.bundler_version,
                        ],
                        capture_output=True,
                        timeout=settings.TIMEOUT_BUNDLER_INSTALL,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_bundler_install")

                self.context.logger.info(
                    f"Bundler version {self.bundler_version} installed"
                )
            else:
                self.context.logger.info(
                    f"Bundler version {self.bundler_version} found"
                )

        if self.jekyll_version is not None:
            gemfile_path = os.path.join(self.context.source_path, "Gemfile")
            if not os.path.exists(gemfile_path):
                self.context.logger.info("No gemfile, init one")
                try:
                    result = subprocess.run(
                        [
                            "scripts/jekyll_init.sh",
                            self.context.source_path,
                            self.bundler_version,
                            self.jekyll_version,
                        ],
                        capture_output=True,
                        timeout=settings.TIMEOUT_JEKYLL_INIT,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_jekyll_init")

            if result.stderr.decode("utf-8") != "":
                raise_error(
                    "error_jekyll_init",
                    stacktrace=result.stderr.decode("utf-8"),
                )

    def build_website(self):
        self.context.logger.info("Installing dependencies")
        try:
            result = subprocess.run(
                [
                    "scripts/bundle_install.sh",
                    self.context.source_path,
                    self.bundler_version,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_BUNDLER_INSTALL,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_install_dependencies")

        if "error" in result.stderr.decode("utf-8").lower():
            raise_error(
                "error_install_dependencies",
                stacktrace=result.stderr.decode("utf-8"),
            )
        self.context.logger.info("Dependencies installed")

        self.context.logger.info("Building website")
        try:
            result = subprocess.run(
                [
                    "scripts/jekyll_build.sh",
                    self.context.source_path,
                    self.bundler_version,
                    self.jekyll_version,
                    self.context.build_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_JEKYLL_BUILD,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_build")

        if "error" in result.stderr.decode("utf-8").lower():
            raise_error(
                "error_build",
                stacktrace=result.stderr.decode("utf-8"),
            )
        if not os.path.exists(self.context.build_path):
            raise_error("unknown_error_build")

        self.context.logger.info("Website built")
