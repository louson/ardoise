"""
This module is used to build a static website with pelican.
"""

import os
import re
import subprocess
import time

import requests

from build.errors import raise_error
import settings


def is_pelican(context):
    for folder in settings.GENERATOR_FOLDERS:
        new_path = os.path.join(context.source_path, folder)
        if os.path.exists(os.path.join(new_path, "pelicanconf.py")):
            context.logger.info(f"Found something to build in '{new_path}'")
            context.source_path = new_path
            return True


class Pelican:
    def __init__(self, context):
        self.name = "pelican"
        self.context = context
        self.python_version = "3"

    def get_versions(self):
        self.pelican_version = settings.PELICAN_DEFAULT_VERSION
        self.python_version = settings.PELICAN_DEFAULT_PYTHON_VERSION
        self.context.logger.info(
            f"Default pelican version to use: {self.pelican_version}"
        )

    def get_build_ready(self):
        pelican_version_path = os.path.join(settings.ARDOISE_TOOLS, "pelican")
        os.makedirs(pelican_version_path, exist_ok=True)
        if self.pelican_version is not None:
            self.context.logger.info("Checking available pelican versions")
            folders = [
                name
                for name in os.listdir(pelican_version_path)
                if os.path.isdir(os.path.join(pelican_version_path, name))
            ]
            pelican_versions = []
            for folder in folders:
                if folder.startswith("pelican_"):
                    pelican_versions.append(folder[8:])
            self.context.logger.info(
                f"Actual pelican versions found: {pelican_versions}"
            )

            if self.pelican_version not in pelican_versions:
                self.context.logger.info(
                    f"Installing pelican version {self.pelican_version}"
                )
                try:
                    result = subprocess.run(
                        [
                            "scripts/pelican_install.sh",
                            pelican_version_path,
                            self.pelican_version,
                        ],
                        capture_output=True,
                        timeout=settings.TIMEOUT_PELICAN_INSTALL,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_pelican_install", self.pelican_version)

                if result.stderr.decode("utf-8") != "":
                    raise_error(
                        "error_pelican_install",
                        self.pelican_version,
                        stacktrace=result.stderr.decode("utf-8"),
                    )

                self.context.logger.info(
                    f"Pelican version {self.pelican_version} installed"
                )
            else:
                self.context.logger.info(
                    f"Pelican version {self.pelican_version} found"
                )

    def build_website(self):
        self.context.logger.info("Building website")
        pelican_version_path = os.path.join(
            settings.ARDOISE_TOOLS,
            "pelican",
            f"pelican_{self.pelican_version}",
        )
        try:
            result = subprocess.run(
                [
                    "scripts/pelican_build.sh",
                    self.context.source_path,
                    pelican_version_path,
                    self.context.build_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_PELICAN_BUILD,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_build")

        if "error" in result.stderr.decode("utf-8").lower():
            raise_error(
                "error_build",
                stacktrace=result.stderr.decode("utf-8"),
            )
        if not os.path.exists(self.context.build_path):
            raise_error("unknown_error_build")

        self.context.logger.info("Website built")
