"""
This module is used to build a static website with hugo.
"""

import os
import re
import subprocess

from build.errors import raise_error
import settings


def is_hugo(context):
    for folder in settings.GENERATOR_FOLDERS:
        new_path = os.path.join(context.source_path, folder)
        if os.path.exists(os.path.join(new_path, "hugo.yml")):
            context.logger.info(f"Found something to build in '{new_path}'")
            context.source_path = new_path
            return True


class Hugo:
    def __init__(self, context):
        self.name = "hugo"
        self.context = context
        self.hugo_version = None

    def get_versions(self):
        self.hugo_version = settings.HUGO_DEFAULT_VERSION
        self.context.logger.info(f"Default hugo version to use: {self.hugo_version}")

    def get_build_ready(self):
        hugo_version_path = os.path.join(settings.ARDOISE_TOOLS, "hugo")
        os.makedirs(hugo_version_path, exist_ok=True)
        if self.hugo_version is not None:
            self.context.logger.info("Checking available hugo versions")
            folders = [
                name
                for name in os.listdir(hugo_version_path)
                if os.path.isdir(os.path.join(hugo_version_path, name))
            ]
            hugo_versions = []
            for folder in folders:
                if folder.startswith("hugo_"):
                    hugo_versions.append(folder[5:])
            self.context.logger.info(f"Actual hugo versions found: {hugo_versions}")

            if self.hugo_version not in hugo_versions:
                self.context.logger.info(f"Installing hugo version {self.hugo_version}")
                try:
                    result = subprocess.run(
                        [
                            "scripts/hugo_install.sh",
                            hugo_version_path,
                            self.hugo_version,
                        ],
                        capture_output=True,
                        timeout=settings.TIMEOUT_HUGO_INSTALL,
                    )
                except subprocess.TimeoutExpired:
                    raise_error("timeout_hugo_install", self.hugo_version)

                if result.stderr.decode("utf-8") != "":
                    raise_error(
                        "error_hugo_install",
                        self.hugo_version,
                        stacktrace=result.stderr.decode("utf-8"),
                    )

                self.context.logger.info(f"Hugo version {self.hugo_version} installed")
            else:
                self.context.logger.info(f"Hugo version {self.hugo_version} found")

            # Install postcss-cli
            try:
                result = subprocess.run(
                    [
                        "scripts/postcss_cli_install.sh",
                        settings.ARDOISE_TOOLS,
                    ],
                    capture_output=True,
                    timeout=settings.TIMEOUT_HUGO_POSTCSS_INSTALL,
                )
            except subprocess.TimeoutExpired:
                raise_error("timeout_postcss_install")

    def build_website(self):
        self.context.logger.info("Building website")
        hugo_version_path = os.path.join(
            settings.ARDOISE_TOOLS,
            "hugo",
            f"hugo_{self.hugo_version}",
        )
        try:
            result = subprocess.run(
                [
                    "scripts/hugo_build.sh",
                    self.context.source_path,
                    hugo_version_path,
                    self.context.build_path,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_HUGO_BUILD,
            )
        except subprocess.TimeoutExpired:
            raise_error("timeout_build")

        if "error" in result.stderr.decode("utf-8").lower():
            raise_error(
                "error_build",
                stacktrace=result.stderr.decode("utf-8"),
            )
        if not os.path.exists(self.context.build_path):
            raise_error("unknown_error_build")

        self.context.logger.info("Website built")
