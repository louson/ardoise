# Ardoise

Ardoise est un outil permettant de publier des sites statiques. L'idée est de simplifier
le travail des personnes souhaitant simplement avoir un site pour eux, une association,
une entreprise, etc...

Le but est également de permettre aux gens de s'extraire des solutions non libres
existantes telles que github.com avec ses github.pages.

L'idée est venue suite à la découverte de
[Scribouilli](https://atelier.scribouilli.org/), outil facilitant la création de sites
webs statiques.


## Fonctionnement

Le site principal permet de saisir l'URL d'un dépôt de code contenant les sources d'un
site statique. Celui-ci va positionner la commande donnée dans une queue prête à prendre
en charge la génération du site.

La génération du site va procéder en plusieurs étapes :

1. L'utilisateur fournit l'URL du dépôt du code source de son site
2. Ardoise teste cette URL afin de définir quel outil de gestion de version est utilisé
3. Une fois trouvé, Ardoise récupère le contenu du dépôt
4. Lorsque le contenu est récupéré, Ardoise analyse celui-ci afin de déterminer quel
   outil de génération est utilisé ainsi que sa version et d'éventuels outils tiers
5. Ardoise installe ensuite les composants demandés si nécessaire
6. Ensuite, le site est généré en utilisant l'outil donné
7. Puis les fichiers du site ainsi généré sont transmis au serveur de sites pour le
   publier
8. Un certificat vers le domaine est généré
9. Le serveur web est mis à jour afin de pointer le nom du site vers les bons fichiers
10. Le site est désormais publié !
11. Le dépôt et les éventuels artefacts temporaires sont détruits


## Pourquoi un tel outil ?

Lorsque des gens souhaitent publier un site web, ils se retrouvent confronter à un souci
pour ce qui concerne l'hébergement. Ils n'ont pas forcément besoin/envie/les moyens de
dépenser/apprendre comment faire.

Il existe cependant déjà des solutions plus ou moins pratiques pour faire héberger du
contenu statique :

* github pages
* readthedocs
* netlify
* surge
* render
* vercel
* kinsta

(liste non exhaustive)

Mais là aussi, on se retrouve face à quelques contraintes et parfois non des moindres :

* le site refuse de publier certains contenus (pas de contenu commercial sur github
  pages, uniquement de la doc sur readthedocs)
* le site demande de créer un compte
* le site contraint à utiliser un certain moteur de site statique, si vous aviez déjà
  construit votre site, il va peut-être falloir recommencer
* quitter le site pour un autre n'est pas toujours simple

Ardoise prend en compte ces points et propose les fonctionnalités suivantes :

* Prendre en compte un dépôt de code pour générer, puis publier le site statique
* Ardoise reconnait plusieurs générateurs de site et le HTML pur (Jekyll et Hugo
  uniquement pour le moment)
* Si un site n'est pas regénéré régulièrement, il est automatiquement détruit
* Seuls les artefacts générés sont conservés et publiés
* Le site est publié à une adresse basée sur le nom du dépôt. Si un dépôt avec le même
  nom existe déjà, le nom est modifiée (ajout de _1, _2, etc...)
* Il est possible de spécifier un nom de domaine personnel (mais ça demande un peu de
  configuration DNS chez votre gestionnaire de domaine)
* Génération de certificats SSL basé sur LetsEncrypt
* Lors de l'analyse du dépôt, le site à générer est cherché à la racine du dépôt, puis
  dans les répertoires "docs" et "doc"
* Spécifier les limites de
    * Taille maximale du site publié (100Mo par défaut)
    * Nombre de publications par 24 heures (5 fois par défaut)
    * Expiration de la génération/publication au-delà d'un certain nombre de minutes (10
      minutes par défaut)


Les fonctionnalités prévues :

* Prise en compte de plus de générateurs de site (Pelican, Mkdocs, Sphinx, Gatsby,
  Docusaurus, ...)
* Création d'un compte permettant de :
    * Ne pas avoir à republier son site pour qu'il soit conservé
    * Pouvoir le régénérer automatiquement via un système de hook
    * Avoir accès à des statistiques de visite du site (si activé)
    * Dépublier/supprimer un site
    * Spécifier une branche du dépôt



