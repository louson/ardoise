#!/bin/bash

cd $1
eval "$(~/.rbenv/bin/rbenv init - bash)"
bundle _$2_ init
bundle _$2_ add jekyll --version "$3"
bundle _$2_ exec jekyll _$3_ new --force --skip-bundle .
echo "Ok"
