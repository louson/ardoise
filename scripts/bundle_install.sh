#!/bin/bash

cd $1
eval "$(~/.rbenv/bin/rbenv init - bash)"
bundle _$2_ install
echo "Ok"
