#!/bin/bash

sudo -u letsencrypt certbot certonly --non-interactive --agree-tos --webroot -w $4/ -d $1
sudo -u letsencrypt cat /etc/letsencrypt/live/$2/fullchain.pem /etc/letsencrypt/live/$2/privkey.pem > $3/$2.pem
echo "Ok"
