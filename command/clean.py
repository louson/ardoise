from datetime import datetime, timedelta
import json
import logging
import os
import shutil
import subprocess

import click
from flask.cli import with_appcontext

import settings


logging.basicConfig(
    filename=settings.LOGFILE,
    encoding="utf-8",
    level=settings.LOGLEVEL,
    format=settings.LOGFORMAT,
    datefmt=settings.LOGDATE,
)
logger = logging.getLogger(__name__)


@click.command(help="Remove websites not updated since too long.")
@with_appcontext
def clean_websites():
    today = datetime.today()
    # List existing websites
    try:
        with open(settings.ARDOISE_NAMES) as file_handler:
            websites = json.load(file_handler)
    except FileNotFoundError:
        websites = {}

    websites_to_delete = []
    for repository, website in websites.items():
        last_update = datetime.strptime(website["last_update"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(days=settings.DAYS_BEFORE_DELETION) < today:
            websites_to_delete.append(
                {
                    "name": website["name"],
                    "repository": repository,
                }
            )

    for website in websites_to_delete:
        name = website["name"]
        domain = f"{name}{settings.ARDOISE_DOMAIN}"
        # Removing datas from names
        try:
            with open(settings.ARDOISE_NAMES) as file_handler:
                names = json.load(file_handler)
        except FileNotFoundError:
            # do nothing
            pass
        try:
            del names[website["repository"]]
        except NameError:
            # do nothing
            pass
        try:
            with open(settings.ARDOISE_NAMES, "w") as file_handler:
                json.dump(names, file_handler, indent=2)
        except FileNotFoundError:
            # do nothing
            pass
        # Removing datas from submissions
        try:
            with open(settings.ARDOISE_SUBMISSIONS) as file_handler:
                submissions = json.load(file_handler)
        except FileNotFoundError:
            # do nothing
            pass
        try:
            del submissions[website["repository"]]
        except NameError:
            # do nothing
            pass
        try:
            with open(settings.ARDOISE_SUBMISSIONS, "w") as file_handler:
                json.dump(submissions, file_handler, indent=2)
        except FileNotFoundError:
            # do nothing
            pass
        # Removing nginx configuration
        try:
            os.remove(os.path.join(settings.ARDOISE_NGINX, f"{name}.conf"))
        except FileNotFoundError:
            # do nothing
            pass
        # Removing published content
        shutil.rmtree(os.path.join(settings.ARDOISE_PUBLISH, name), ignore_errors=True)
        # Removing certificate
        try:
            os.remove(os.path.join(settings.ARDOISE_CERTBOT, f"{domain}.pem"))
        except FileNotFoundError:
            # do nothing
            pass
        try:
            result = subprocess.run(
                [
                    "scripts/certbot_delete.sh",
                    domain,
                ],
                capture_output=True,
                timeout=settings.TIMEOUT_CERTIFICATE,
            )
        except subprocess.TimeoutExpired:
            # TODO: manage this
            pass

        logger.info(f"{name} website removed.")

    try:
        with open(settings.ARDOISE_SUBMISSIONS) as file_handler:
            repositories = json.load(file_handler)
    except FileNotFoundError:
        repositories = {}
    repositories_to_delete = []
    for repository, submissions in repositories.items():
        last_update = datetime.strptime(
            submissions[-1]["submission"],
            "%Y-%m-%d %H:%M:%S",
        )
        if last_update + timedelta(days=settings.DAYS_BEFORE_DELETION) < today:
            repositories_to_delete.append(repository)
    for repository in repositories_to_delete:
        del repositories[repository]
    try:
        with open(settings.ARDOISE_SUBMISSIONS, "w") as file_handler:
            json.dump(repositories, file_handler, indent=2)
    except FileNotFoundError:
        # do nothing
        pass
