import json
import random
import logging

from flask import current_app, Flask, g, redirect, render_template, request, url_for
from redis import Redis
import rq

from command import commands
import settings

app = Flask(__name__)
app.redis = Redis.from_url(settings.REDIS_URL)
app.queue = rq.Queue("ardoise", connection=app.redis)
app.config.version = settings.ARDOISE_VERSION

for command in commands:
    app.cli.add_command(command)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        name = request.form["repository"]
        identifier = f"{random.getrandbits(128):x}"
        logging.basicConfig(
            filename=settings.LOGFILE,
            encoding="utf-8",
            level=settings.LOGLEVEL,
            format=settings.LOGFORMAT,
            datefmt=settings.LOGDATE,
        )
        logger = logging.getLogger(__name__)
        logger.info(f"INFO - {name} - {identifier} - Mise en queue")
        job = current_app.queue.enqueue(
            "build.build.build",
            args=(name, identifier),
            job_timeout=settings.TIMEOUT_JOB,
        )
        return redirect(url_for("build", token=job.get_id()))

    return render_template("index.html")


@app.route("/stats")
def stats():
    try:
        with open(settings.ARDOISE_STATS_HOURS) as file_handler:
            g.hours_stats = json.load(file_handler)
    except FileNotFoundError:
        g.hours_stats = []

    try:
        with open(settings.ARDOISE_STATS_DAYS) as file_handler:
            g.days_stats = json.load(file_handler)
    except FileNotFoundError:
        g.days_stats = []

    return render_template("stats.html")


@app.route("/faq")
def faq():
    return render_template("faq.html")


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/build/<token>")
def build(token):
    return render_template("building.html", token=token)


@app.route("/state/<token>")
def state(token):
    try:
        job = rq.job.Job.fetch(token, connection=current_app.redis)
    except rq.exceptions.NoSuchJobError:
        return {"logs": [], "stop": True}

    return {"logs": job.meta.get("logs", []), "stop": job.meta.get("stop", False)}
