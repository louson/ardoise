# Bienvenue sur la documentation d'Ardoise

Ardoise est un outil permettant de publier des sites statiques. L'idée est de simplifier
le travail des personnes souhaitant simplement avoir un site pour eux, une association,
une entreprise, etc...

Le but est également de permettre aux gens de s'extraire des solutions non libres
existantes telles que github.com avec ses github.pages.


Cette documentation tâche d'expliquer comment tout fonctionne et comment installer le
service.


## Fonctionnement

### Génération des sites statiques

Le site principal permet de saisir une l'URL d'un dépôt de code contenant les sources
d'un site statique. Celui-ci va positionner la commande donnée dans une queue prête à
prendre en charge la génération du site.

La génération du site va procéder en plusieurs étapes :

1. L'utilisateur fournit l'URL du dépôt du code source de son site
2. Ardoise teste cette URL afin de définir quel outil de gestion de version est utilisé
3. Une fois trouvé, Ardoise récupère le contenu du dépôt
4. Lorsque le contenu est récupéré, Ardoise analyse celui-ci afin de déterminer quel
    outil de génération est utilisé ainsi que sa version et d'éventuels outils tiers
5. Ardoise installe ensuite les composants demandés si nécessaire
6. Ensuite, le site est généré en utilisant l'outil donné
7. Puis les fichiers du site ainsi généré sont transmis au serveur de sites pour le
    publier
8. Un certificat vers le domaine est généré
9. Le serveur web est mis à jour afin de pointer le nom du site vers les bons fichiers
10. Le site est désormais publié !
11. Le dépôt et les éventuels artefacts temporaires sont détruits


## Installation

### Système

(ces commandes doivent être exécutées avec l'utilisateur root)

Pour simplifier, seul Debian est prs en compte actuellement. Les paquets nécessaires
doivent être installés sur le système.

    sudo apt-get install python3-venv ruby rbenv nginx redis git haproxy incron

On peut tout de suite spécifier l'utilisateur root pour utiliser incron :

    echo "root" > /etc/incron.allow

Il faut ensuite créer un utilisateur spécifique pour séparer proprement les droits.

    useradd --system -m -g www-data ardoise

Ici, on conserve le répertoire _home_ par défaut : /home/ardoise
L'utilisateur est dans le groupe ardoise


### Structure des répertoires

(ces commandes doivent être exécutées avec l'utilisateur root)

Maintenant que l'utilisateur est créé, il est possible de mettre en place la structure
des répertoires qui va être utilisée :

    install -dm0770 -o ardoise -g www-data /var/ardoise/{www,errors}
    install -dm0770 -o ardoise /var/ardoise/{nginx,certs,tools}
    install -dm0770 -o ardoise /var/log/ardoise

* Le répertoire _www_ contient les sites générés
* Le répertoire _errors_ contient les pages d'erreur par défaut
* Le répertoire _nginx_ contient les fichiers de configuration des sous-domaines
    * Ce répertoire est lié à un sous-répertoire des sites actifs de nginx pour leur
    publication
* Le répertoire _letsencrypt_ contient les données de vérification générées par cerbot
  pour générer les certificats
* Le répertoire _certs_ contient les certificats SSL des sites générés

### Mise en place de LetsEncrypt

(ces commandes doivent être exécutées avec l'utilisateur root)

LetsEncrypt est un outil permettant de générer des certificats pour faire du HTTPS. Le
souçis c'est qu'en général il est utilisé par l'utilisateur root. Sachant qu'on souhaite
l'utiliser de manière automatique, on va d'abord permettre à un utilisateur spécifique
de l'utiliser, puis on autorisera l'utilisateur ardoise à utiliser la commande certbot
pour gérer les certificats.

On crée donc d'abord l'utilisateur letsencrypt et le groupe correspondant :

    useradd -r letsencrypt

On créée les répertoires nécessaires :

    install -dm0770 -o letsencrypt -g letsencrypt /etc/letsencrypt
    install -dm0770 -o letsencrypt /var/{log,lib}/letsencrypt
    install -dm0770 -o ardoise -g letsencrypt /var/ardoise/letsencrypt

On va ensuite permettre à l'utilisateur ardoise d'utiliser la commande certbot via
l'utilisateur letsencrypt, mais également de lire les fichiers pour pouvoir copier les
certificats ailleurs pour haproxy. Pour cela, on utilise visudo afin d'ajouter les
lignes suivantes au fichier de configuration sudoers :

    ardoise ALL=(letsencrypt:letsencrypt) NOPASSWD:/usr/bin/certbot
    ardoise ALL=(letsencrypt:letsencrypt) NOPASSWD:/bin/cat

On peut vérifier que tout est ok en affichant la version de certbot depuis l'utilisateur
ardoise :

    su ardoise
    sudo -u letsencrypt certbot --version


### Installation de l'application

(ces commandes doivent être exécutées avec l'utilisateur ardoise)

Il suffit de récupérer le dépôt de code dont fait partie cette documentation.

Le site web public fonctionne en _Python3_ avec le framework _Flask_ et un gestionnaire
de queue basé sur _Redis_. C'est cette queue qui reçoit toutes les demandes de
génération de site.

On récupère d'abord le site lui-même :

    cd ~
    git clone https://framagit.org/ardoise/website.git website

Afin de ne pas interférer avec le système, on travaille dans un environnement virtuel
pour installer les dépendances :

    cd ~/website
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt

Il est ensuite possible, voire nécessaire de créer un fichier _.venv_ afin de déterminer
la configuration de l'application. Typiquement, le domaine à utiliser, voire le niveau
de log attendu.

    cp sample.env .env


#### nginx

(ces commandes doivent être exécutées avec l'utilisateur ardoise)

Les sites générés vont chacun avoir leur propre fichier de configuration nginx. Ces
fichiers sont créés dynamiquement à la fin de la première génération de chaque site. Les
fichiers de configuration sont stockés dans le répertoire /var/ardoise/nginx. Celui-ci
doit donc être reconnu par nginx. On va donc créer un lien symbolique de ce répertoire
vers nginx, et adapter la configuration de nginx en conséquence :

    ln -s /var/ardoise/nginx /etc/nginx/sites-enabled/ardoise

Puis on édite le fichier de configuration de nginx /etc/nginx/nginx.conf afin d'y
ajouter une option _include_ car nginx ne va pas chercher les fichiers de configuratoin
dans les sous-répertoires :

    include /etc/nginx/sites-enabled/ardoise/*.conf

On copie la configuration de base du site dans le répertoire des sites actifs de nginx :

    cp /home/ardoise/website/docs/configuration/nginx/ardoise.conf /etc/nginx/sites-enabled/

Puis on le modifie comme souhaité en l'éditant, typiquement en spécifiant le nom de
domaine à utiliser.

Afin de pouvoir générer des certificats LetsEncrypt, on va également mettre en place un
site statique simple permettant à certbot de vérifier que la machine est bien celle liée
au nom de domaine du certificat à générer :

    cp /home/ardoise/website/docs/configuration/nginx/certbot.conf /etc/nginx/sites-enabled/

Il n'est normalement pas utile de modifier ce fichier.

On peut alors tester, puis relancer nginx si aucune erreur de configuration n'a été
relevée :

    nginx -t
    nginx -s reload

Afin de ne pas avoir à recharger soi-même nginx à chaque fois qu'un site est créé, on va
utiliser la librairie inotify via incron. Il suffit d'éditer la table via la commande
_incrontab -e_ et d'y ajouter la ligne suivante :

    /var/ardoise/nginx/   IN_CREATE,IN_DELETE   nginx -s reload

Désormais, lorsqu'un nouveau site est créé, nginx est rechargé et repère le nouveau
site. Idem pour les nettoyages des anciens sites.


#### haproxy

(ces commandes doivent être exécutées avec l'utilisateur ardoise)

Haproxy est placé en amont des requêtes afin de les rediriger. Il va également permettre
de gérer la partie SSL car nginx prend a priori beaucoup plus de mémoire s'il s'en
occupe.

Haproxy va également servir à rediriger correctement le trafic de certbot lors de la
génération des certificats.

La configuration a positionner dans le fichier /etc/haproxy/haproxy.cfg est disponible
dans le fichier /home/ardoise/website/docs/configuration/haproxy/haproxy.cfg.

On va d'abord configurer la partie pour certbot, autrement dit le frontend _http-in_ et
le backend _backcertbot_.

Une fois cela fait, on va pouvoir générer un certificat pour le site principal :

    certbot certonly --non-interactive --agree-tos --webroot -w /var/ardoise/letsencrypt/ -d croqu.is -d www.croqu.is

Une fois le certificat produit, il faut concaténer deux de ses fichiers pour fournir un
certificat à haproxy. **Attention** de bien remplacer le nom du domaine par celui que
vous utilisez :

    cat /etc/letsencrypt/live/example.org/fullchain.pem /etc/letsencrypt/live/example.org/privkey.pem > /var/ardoise/certs/exampleorg.pem

On peut maintenant finaliser la configuration de Haproxy (qui a besoin d'au moins un
certificat SSL pour démarrer. On rajoute donc les configurations frontend https-in et
backend server.

Une fois correctement configuré, on peut redémarrer haproxy. Désormais, toute requête
ne pointant pas exactement sur l'attendu de certbot sera redirigé vers le protocole
sécurisé https, puis le certificat sera alors vérifié. Si tout est ok, le flux sera
simplement redirigé vers nginx.

Afin de ne pas avoir à recharger soi-même haproxy à chaque fois qu'un certificat est
créé, on va utiliser la librairie inotify via incron. Il suffit d'éditer la table via la
commande _incrontab -e_ et d'y ajouter la ligne suivante :

    /var/ardoise/certs/   IN_CREATE,IN_DELETE   systemctl reload haproxy

Désormais, lorsqu'un nouveau certificat est créé, haproxy est rechargé et repère le
nouveau certificat. Idem pour les nettoyages des anciens certificats.


#### systemd

(ces commandes doivent être exécutées avec l'utilisateur ardoise)

Il faut maintenant démarrer les deux applications : le site principal et la queue
chargée de générer les sites statiques des utilisateurs.

Les fichiers nécessaires sont situés dans
/home/ardoise/website/docs/configuration/systemd.

On copie donc ces deux fichiers :

    cp /home/ardoise/website/docs/configuration/systemd/*.service /etc/systemd/system/

Il n'est normalement pas utile de les modifier, sauf si l'installation a été faite
différemment depuis le départ.

On recharge ensuite la liste des services de systemd avant de démarrer ceux-ci :

    systemctl daemon-reload
    systemctl start ardoise.service
    systemctl start ardoise-rq.service


### Installation de ruby/rbenv pour jekyll

(ces commandes doivent être exécutées avec l'utilisateur ardoise)

_rbenv_ est un outil permettant de gérer différentes versions de _ruby_. Il est donc
nécessaire pour utiliser les différentes versions de _jekyll_.

On commence par installer (en tant que root), les composants systèmes nécessaires à son
utilisation :

    apt-get install git build-essential zlib1g-dev libreadline-dev libssl-dev libcurl4-openssl-dev

Puis, on suit les directives du projet :

    cd ~
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv

On active _rbenv_ pour l'utilisateur.

    echo 'eval "$(~/.rbenv/bin/rbenv init - bash)"' >> ~/.bashrc
    source .bashrc

Enfin, on installe le plugin ruby-build.

    git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

Les versions de ruby seront installées dans le répertoire dédié /var/ardoise/tools. On
peut installer quelques versions de ruby en avance car c'est un processus plutôt long :

    cd /var/ardoise/tools
    mkdir jekyll
    cd jekyll
    rbenv install 3.1.2
    rbenv install 2.7.3


## Statistiques

Les statistiques sont calculés de manière horaire et journalière. Les scripts permettant
de lancer ces calculs sont dans le répertoire _crontab_ de la documentation. Il faut les
copier dans le répertoire _/var/ardoise/tools_, puis leur donner le droit d'être
exécuté :


    cp /home/ardoise/website/docs/configuration/crontab/*.sh /var/ardoise/tools/
    cd /var/ardoise/tools/
    chmod +x *.sh

Puis on édite le crontab de l'utilisateur afin d'appeler ces scripts :

    10 * * * *   /var/ardoise/tools/stats-hour.sh
    10 0 * * *  /var/ardoise/tools/stats-day.sh
